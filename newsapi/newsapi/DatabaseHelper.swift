//
//  DatabaseHelper.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
public class DatabaseHelper {
    
    public static func writeArticleInfo(article: Article) {
        print("add")
        if(!article.isInvalidated ){
            try! uiRealm.write {
                uiRealm.add(article, update: .modified)
            }
        }
    }
    public static func updateAllArticles(articles: [Article]) {
        print("upd")
        try! uiRealm.write {
            uiRealm.create(Article.self, value: articles, update: .all)
        }
    }
    public static func deleteAllFromFavorites() {
        print("del")
        try! uiRealm.write {
            uiRealm.deleteAll()
        }
    }
  
    public static func getArticleInfo() -> [Article]{
        let articles = uiRealm.objects(Article.self)
        return Array(articles)
    }
}
