//
//  FavoritesTableViewController.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {
    var articles = [Article]()
    var pageNumber = 1
    var isDonePaginating = false
    let progressIndicator = ProgressIndicator(text: "Пожалуйста, подождите ...")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        articles = DatabaseHelper.getArticleInfo()
        articles = articles.filter {$0.title != ""}
        UIApplication.shared.keyWindow?.addSubview(progressIndicator)
        UIApplication.shared.keyWindow?.bringSubviewToFront(progressIndicator)
        progressIndicator.hide()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        articles = DatabaseHelper.getArticleInfo()
        articles = articles.filter {$0.title != ""}
        
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "newsDetail") as? NewsDetailViewController
        vc?.article = articles[indexPath.row]
        self.present(vc!, animated: true, completion: nil)
        
        
    }
    
    var categoryIdAll : Int? = nil
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ident", for: indexPath) as! NewsTableViewCell
        let article = articles[indexPath.row]
        cell.saveToFav.tag = indexPath.row
        cell.vidimg.loadImageUsingUrlString(urlString: article.urlToImage ?? SettingsConstant.noImg)
        cell.dataLbl.text = article.publishedAt?.convertDate()
        cell.vidlbl.text = article.author
        cell.titleLbl.text = article.title
        //  cell.saveToFav.addTarget(self, action: #selector(saveToFavorites(_:)), for: .touchUpInside)
        
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
}

extension String {
    
    func convertDate() -> String? {
        let dates = self.split(separator: "T")
        let time = ((dates[1] as NSString).substring(with: NSMakeRange(0, 5)) as NSString)
        return String("\(dates[0]) \(time)")
    }
    
    
}
