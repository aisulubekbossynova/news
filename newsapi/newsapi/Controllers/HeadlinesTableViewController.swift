//
//  HeadlinesTableViewController.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/19/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class HeadlinesTableViewController: UITableViewController {
    
    var articles = [Article]()
    var pageNumber = 0
    var isDonePaginating = false
    let progressIndicator = ProgressIndicator(text: "Пожалуйста, подождите ...")
    var firstList = [Article]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchFirstTime()
        UIApplication.shared.keyWindow?.addSubview(progressIndicator)
        UIApplication.shared.keyWindow?.bringSubviewToFront(progressIndicator)
        progressIndicator.hide()
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
            self.checkNews(pageNumber: 0, pageSize: 15, keyword: SettingsConstant.keyword)
        }
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "newsDetail") as? NewsDetailViewController
        vc?.article = articles[indexPath.row]
        self.present(vc!, animated: true, completion: nil)
        
        
    }
    
    var categoryIdAll : Int? = nil
    var selectedIndexPaths = NSMutableSet()
    
    
    var favorites = DatabaseHelper.getArticleInfo()
    @objc func saveToFavorites(_ sender: UIButton) {
        let buttonPostion = sender.convert(sender.bounds.origin, to: tableView)
        
        if let indexPath = tableView.indexPathForRow(at: buttonPostion) {
            let cell = tableView.cellForRow(at: indexPath) as! NewsTableViewCell
            if(cell.cellselected){
                favorites = favorites.filter {$0.title != articles[sender.tag].title && $0.title != ""}
                if(favorites.count != 0){
                //DatabaseHelper.deleteAllFromFavorites()
                //DatabaseHelper.updateAllArticles(articles: favorites)
                    sender.setImage(UIImage(named: "nofill_star"), for: .normal)
                    
                }
            }
            else{
                let articleObj = favorites.filter{ $0.title == articles[sender.tag].title }.first
                if(articleObj == nil){
                    DatabaseHelper.writeArticleInfo(article: articles[sender.tag])
                    sender.setImage(UIImage(named: "Shape"), for: .normal)
                }
                
            }
            cell.cellselected = !cell.cellselected
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ident", for: indexPath) as! NewsTableViewCell
        let article = articles[indexPath.row]
        cell.saveToFav.tag = indexPath.row
        cell.vidimg.loadImageUsingUrlString(urlString: article.urlToImage ?? SettingsConstant.noImg)
        cell.dataLbl.text = article.publishedAt?.convertDate()
        cell.vidlbl.text = article.author
        cell.titleLbl.text = article.title
        cell.cellselected = false
        let articleObj = favorites.filter{ $0.title == article.title }.first
        
        if(articleObj != nil){
            //cell.saveToFav.isSelected = true
            cell.cellselected = true
        }
        cell.saveToFav.addTarget(self, action: #selector(saveToFavorites(_:)), for: .touchUpInside)
        if(cell.cellselected){
            cell.saveToFav.setImage(UIImage(named: "Shape"), for: .normal)
        }
        else{
            cell.saveToFav.setImage(UIImage(named: "nofill_star"), for: .normal)
        }
        if(indexPath.row == articles.count - 1 && !isDonePaginating){
            pageNumber += 1
            loadNewsList(pageNumber: pageNumber, pageSize: 15, keyword: SettingsConstant.keyword)
            
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    fileprivate func fetchFirstTime() {
        loadNewsList(pageNumber: pageNumber, pageSize: 15, keyword: SettingsConstant.keyword)
        
    }
    func checkNews(pageNumber: Int, pageSize: Int, keyword: String) {
        progressIndicator.show()
        ApiService.shared.getTopHeadlines(key: keyword, pageSize: pageSize, page: pageNumber){
            (newsInfo, errorResponse) in
            if(newsInfo != nil && errorResponse == nil){
                self.progressIndicator.hide()
                if(!(newsInfo?.articles.elementsEqual(self.firstList))!){
                    self.articles = (newsInfo?.articles ?? []) as [Article]
                    self.articles = self.articles.filter {$0.title != ""}
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            else{
                self.progressIndicator.hide()
                ExceptionHelper.showErrorMessage(messageTitle: NSLocalizedString("Failed.", comment: ""), errorMessage: errorResponse?.message ?? NSLocalizedString("The Internet connection appears to be offline.", comment: ""), buttonText: "Ok", parent: self)
                
            }
        }
    }
    func loadNewsList(pageNumber: Int, pageSize: Int, keyword: String) {
        progressIndicator.show()
        ApiService.shared.getTopHeadlines(key: keyword, pageSize: pageSize, page: pageNumber){
            (newsInfo, errorResponse) in
            if(newsInfo != nil && errorResponse == nil){
                self.progressIndicator.hide()
                self.articles.append(contentsOf: newsInfo?.articles ?? [])
                self.articles = self.articles.filter {$0.title != ""}
                print( newsInfo?.articles ?? [])
                if(self.articles.count == 15){
                    self.firstList.append(contentsOf: self.articles)
                }
                if((newsInfo?.totalResults)! <= self.articles.count){
                    self.isDonePaginating = true
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            else{
                self.progressIndicator.hide()
                ExceptionHelper.showErrorMessage(messageTitle: NSLocalizedString("Failed.", comment: ""), errorMessage: errorResponse?.message ?? NSLocalizedString("The Internet connection appears to be offline.", comment: ""), buttonText: "Ok", parent: self)
                
            }
        }
    }
    
    
    
    
}
