//
//  HeadlinesTableViewController.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/19/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class EverythingTableViewController: UITableViewController {
    var articles = [Article]()
    var pageNumber = 1
    var refreshController = UIRefreshControl()
    
    var isDonePaginating = false
    let progressIndicator = ProgressIndicator(text: "Пожалуйста, подождите ...")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchFirstTime()
        UIApplication.shared.keyWindow?.addSubview(progressIndicator)
        UIApplication.shared.keyWindow?.bringSubviewToFront(progressIndicator)
        progressIndicator.hide()
        
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshController)
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        fetchFirstTime()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "newsDetail") as? NewsDetailViewController
        vc?.article = articles[indexPath.row]
        self.present(vc!, animated: true, completion: nil)
    }
    
    var categoryIdAll : Int? = nil
    var selectedIndexPaths = NSMutableSet()
    
    var favorites = DatabaseHelper.getArticleInfo()
    @objc func saveToFavorites(_ sender: UIButton) {
        if(sender.isSelected){
            print("delete \(articles[sender.tag].title)")
            favorites = favorites.filter {$0.title == articles[sender.tag].title && $0.title != ""}
           // DatabaseHelper.deleteAllFromFavorites()
           // DatabaseHelper.updateAllArticles(articles: favorites)
            sender.setImage(UIImage(named: "nofill_star"), for: .normal)
        }
        else{
            print("add \(articles[sender.tag].title)")
            DatabaseHelper.writeArticleInfo(article: articles[sender.tag])
            sender.setImage(UIImage(named: "Shape"), for: .normal)
            
        }
        sender.isSelected = !sender.isSelected
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ident", for: indexPath) as! NewsTableViewCell
        let article = articles[indexPath.row]
        cell.saveToFav.tag = indexPath.row
        cell.vidimg.loadImageUsingUrlString(urlString: article.urlToImage ?? SettingsConstant.noImg)
        cell.dataLbl.text = article.publishedAt?.convertDate()
        cell.vidlbl.text = article.author
        cell.titleLbl.text = article.title
        let articleObj = favorites.filter{ $0.title == article.title }.first
        
        if(articleObj != nil){
            cell.saveToFav.isSelected = true
            cell.cellselected = true
        }
        cell.saveToFav.addTarget(self, action: #selector(saveToFavorites(_:)), for: .touchUpInside)
        if(cell.cellselected){
            cell.saveToFav.setImage(UIImage(named: "Shape"), for: .normal)
        }
        else{
            cell.saveToFav.setImage(UIImage(named: "nofill_star"), for: .normal)
        }
        if(indexPath.row == articles.count - 1 && !isDonePaginating){
            pageNumber += 1
            loadNewsList(pageNumber: pageNumber, pageSize: 15, keyword: SettingsConstant.keyword)
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    fileprivate func fetchFirstTime() {
        loadNewsList(pageNumber: pageNumber, pageSize: 15, keyword: SettingsConstant.keyword)
        
    }
    
    func loadNewsList(pageNumber: Int, pageSize: Int, keyword: String) {
        progressIndicator.show()
        ApiService.shared.getEverything(key: keyword, pageSize: pageSize, page: pageNumber){
            (newsInfo, errorResponse) in
            if(newsInfo != nil && errorResponse == nil){
                self.progressIndicator.hide()
                self.articles.append(contentsOf: newsInfo?.articles ?? [])
                print( newsInfo?.articles ?? [])
                if((newsInfo?.totalResults)! <= self.articles.count){
                    self.isDonePaginating = true
                }
                self.refreshController.endRefreshing()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            else{
                self.progressIndicator.hide()
                ExceptionHelper.showErrorMessage(messageTitle: NSLocalizedString("Failed.", comment: ""), errorMessage: errorResponse?.message ?? NSLocalizedString("The Internet connection appears to be offline.", comment: ""), buttonText: "Ok", parent: self)
                
            }
        }
    }
    
    
}



let imageCache = NSCache<AnyObject, AnyObject>()
class CustomImageView: UIImageView{
    var imgUrlString: String?
    func loadImageUsingUrlString(urlString: String){
        imgUrlString = urlString
        let url = URL(string: urlString)
        image = nil
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: ((url ?? URL(string: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.levelupsneakers.com%2Fc%2Fnike-3&psig=AOvVaw1Cv9Wdce_Hp-MKSmb2XL_l&ust=1595354069815000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPiImZez3OoCFQAAAAAdAAAAABAI"))! ))  { data, response, error in
            if error != nil {
                return
            }
            DispatchQueue.main.async() {
                let imageToCache = UIImage(data: data!)
                if(self.imgUrlString == urlString){
                    self.image = imageToCache
                }
                imageCache.setObject(imageToCache as AnyObject, forKey: urlString as AnyObject)
            }
        }.resume()
    }
}
// TODO: -add localizable
//"Result data is nil" = "Не найдено никаких данных.";
//"Network error" = "Похоже, возникла проблема с интернетом.";
//"The Internet connection appears to be offline." = "Похоже, возникла проблема с интернетом.";
//"Unrecognized error" = "Произошла неизвестная ошибка.";
//"Failed." = "Ошибка";
