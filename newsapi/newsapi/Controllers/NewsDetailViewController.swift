//
//  NewsDetailViewController.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentTV: UITextView!
    @IBOutlet weak var imgView: CustomImageView!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    var article : Article!
    override func viewDidLoad() {
        super.viewDidLoad()
        setbackButton()
        titleLbl.text = article.title
        contentTV.text = article.content
        authorLbl.text = article.author
        dateLbl.text = article.publishedAt?.convertDate()
        imgView.loadImageUsingUrlString(urlString: article.urlToImage ?? SettingsConstant.noImg)

        // Do any additional setup after loading the view.
    }
    
    
    func setbackButton() {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.setImage(UIImage(named: "close"), for: .normal)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
       
    }
    
    @objc func handleBack() {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
