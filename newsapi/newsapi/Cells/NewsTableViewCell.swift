//
//  NewsTableViewCell.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    @IBOutlet weak var vidlbl: UILabel!
    @IBOutlet weak var vidimg: CustomImageView!
    @IBOutlet weak var dataLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var saveToFav: UIButton!
    var cellselected: Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        cellselected = false
        saveToFav.setImage(UIImage(named: "nofill_star"), for: .normal)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
