//
//  Article.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
public class Article: Object, Codable {
    @objc dynamic public var author : String?
    @objc dynamic public var title : String = ""
    //@objc dynamic public var description : String = ""
    @objc dynamic public var url : String?
    @objc dynamic public var urlToImage : String?
    @objc dynamic public var publishedAt : String?
    @objc dynamic public var content : String?
    @objc dynamic public var source : Source?
    //dynamic public var highlighted = false
    public override class func primaryKey() -> String {
        return "title"
    }
    public override static func ignoredProperties() -> [String] {
           return ["highlighted"]
       }
    convenience init (author : String, title: String,  url: String, urlToImage: String, publishedAt: String, content: String, source: Source ) {
        self.init()
        self.author = author
        self.title = title
      //  self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
        self.content = content
    }
    required init(){
        super.init()
        
    }
    
    init?(json: [String: Any]) {
        super.init()
        let _author = json["author"] as? String ?? ""
        let _title = json["title"] as? String ?? ""
      //  let _description = json["description"] as? String ?? ""
        let _url = json["url"] as? String ?? ""
        let _urlToImage = json["urlToImage"] as? String ?? ""
        let _publishedAt = json["publishedAt"] as? String ?? ""
        let _content = json["content"] as? String ?? ""
        let _source = json["source"] as? Source ?? nil
        self.author = _author
        self.title = _title
        self.publishedAt = _publishedAt
        self.url = _url
        self.urlToImage = _urlToImage
        self.content = _content
        self.source = _source
    
       
    }
   
    
}
