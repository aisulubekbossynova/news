//
//  ErrorResponse.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import UIKit
struct ErrorResponse: Error, Decodable  {
    
    let message: String
    let code: Int
    let status: Int
    
    init() {
        self.message = "Произошла неизвестная ошибка. Попробуйте сделать Ваш запрос позже";
        self.code = 0;
        self.status = 0;
    }
    
    init( message: String, code: Int, status: Int) {
        self.message = message;
        self.code = code;
        self.status = status;
    }
    
    init?(json: [String: Any]) {
        let _message = json["message"] as? String
        let _code = json["code"] as? Int
        let _status = json["status"] as? Int
        self.message = _message ?? ""
        self.code = _code ?? 0
        self.status = _status ?? 0
     
    }
    private enum CodingKeys: String, CodingKey {
        case message
        case code
        case status
    }
    
    var localizedDescription: String {
        return message
    }
}

public class ExceptionHelper {
    public static func showErrorMessage(messageTitle: String, errorMessage: String, buttonText: String, parent: UIViewController) {
        let alertController = UIAlertController(title: messageTitle, message: errorMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: buttonText, style : .default))
        parent.present(alertController, animated: true, completion: nil)
    }
  
}
