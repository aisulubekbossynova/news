//
//  NewsResponse.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
//
//struct NewsResponse: Decodable {
//    let status: String
//    let totalResults: Int
//    let articles: [Article]
//
//}


public final class NewsResponse: Object, Codable {
    @objc dynamic public var status : String?
    @objc dynamic var totalResults : Int = 0
    public var articles : [Article] = []

    convenience init (status : String, totalResults: Int, articles: [Article]) {
        self.init()
        self.status = status
        self.totalResults = totalResults
        self.articles = articles

    }
    required init(){
        super.init()

    }

    init?(json: [String: Any]) {
        super.init()
        let _status = json["status"] as? String ?? nil
        let _totalResults = json["totalResults"] as? Int ?? 0
        let _articles = json["articles"] as? [Article] ?? []
        self.status = _status
        self.totalResults = _totalResults
        self.articles = _articles

    }


}
