//
//  Source.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
public class Source: Object, Codable {
    @objc dynamic public var id : String?
    @objc dynamic public var name : String?
   
    
    convenience init (id : String, name: String) {
        self.init()
        self.id = id
        self.name = name
        
    }
    required init(){
        super.init()
        
    }
    
    init?(json: [String: Any]) {
        super.init()
        let _id = json["id"] as? String ?? nil
        let _name = json["name"] as? String ?? nil
        self.id = _id
        self.name = _name
       
    }
 
    
}
