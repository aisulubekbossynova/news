//
//  ApiService.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Moya
class ApiService {
    
    static let shared =  ApiService()
    let newsServiceProvider = MoyaProvider<NewsService>()
    
    func getTopHeadlines(key: String, pageSize: Int, page: Int,  completion:@escaping (NewsResponse?, ErrorResponse?) -> () ){
        let requestHeadlines = NewsService.getTopHeadlines(key: key, pageSize: pageSize, page: page)
        
        return fetchData(service: requestHeadlines, completion: completion)
    }
    
    func getEverything(key: String, pageSize: Int, page: Int,   completion:@escaping ( NewsResponse? , ErrorResponse?) -> () ){
        let requestEver = NewsService.getEverything(key: key, pageSize: pageSize, page: page)
        
        return fetchData(service: requestEver, completion: completion)
    }
    
    func fetchData<NewsResponse: Decodable> (service: NewsService, completion:@escaping (NewsResponse?, ErrorResponse?) -> ()) {
        newsServiceProvider.request(service) { result in
            switch result {
                case let .success(moyaResponse):
                    if(moyaResponse.statusCode == 200){
                        do{
                     //   completion(self.decodedObject(object: moyaResponse.data), nil)
                        let newsJson = try JSONDecoder().decode(NewsResponse.self, from: moyaResponse.data)
//                            let articleList = newsJson.
//                        let news = NewsResponse
                            completion(newsJson, nil)
                            
                        }
                        catch {
                            print()
                        }
                    }
                    else{
                        
                        completion(nil, self.decodedErrorObject(object: moyaResponse.data))
                        
                    }
                    
                    break
                case let .failure(error):
                    completion(nil, self.convertToError(object: error))
                    break
            }
        }
        
        
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            print("Failed to download data")
        }else {
            print("Data downloaded")
            
        }
    }
    
    
    func convertToError (object: MoyaError) -> ErrorResponse?{
        if(object.errorDescription == "URLSessionTask failed with error: The request timed out."){
            let errorResponse = ErrorResponse( message:  NSLocalizedString("Интернет соединение прервано", comment: ""), code: 0, status: object.errorCode)
            return errorResponse
        }
        else{
            let errorResponse = ErrorResponse(message: object.errorDescription ?? NSLocalizedString("Unrecognized error", comment: ""), code: 0, status: object.errorCode)
            return errorResponse
        }
        
    }
    func decodedErrorObject (object: Data) -> ErrorResponse?{
        guard let json = try? JSONSerialization.jsonObject(with: object, options: .mutableContainers) as? NSDictionary
            else { return nil }
        let errorResponse = ErrorResponse(json: json as! [String : Any])
        return errorResponse
    }
    
    func decodedObject<T: Decodable> (object: Data) -> T?{
        do{
            let jsonDecoder = JSONDecoder()
            let errorResponse = try jsonDecoder.decode(T.self, from: object)
            return errorResponse
        }
        catch{
            return nil
        }
    }
}
