//
//  NewsService.swift
//  newsapi
//
//  Created by aisulubekbossynova on 7/20/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import Moya

enum NewsService {
    case getTopHeadlines (key: String, pageSize: Int, page: Int)
    case getEverything (key: String, pageSize: Int, page: Int)
    
    
}

extension NewsService: TargetType {
    
    var baseURL: URL {
        switch self {
            case .getTopHeadlines, .getEverything:
                guard let url = URL(string: SettingsConstant.baseApi) else { fatalError(NSLocalizedString("Base auth url is not configured", comment: "Base url")) }
                return url
            
        }
    }
    
    var path: String {
        switch self {
            case .getTopHeadlines:
                return "v2/top-headlines"
            case .getEverything:
                return "v2/everything"
        }
    }
    
    var method: Moya.Method {
        switch self {
            case .getTopHeadlines:
                return .get
            case .getEverything :
                return .get
        }
    }
    
    var task: Task {
        var taskValue = [String: Any]();
        switch self {
            case .getTopHeadlines(let key, let pageSize, let page), .getEverything(let key, let pageSize, let page):
                taskValue["q"] = key
                taskValue["apiKey"] = SettingsConstant.apiKey
                taskValue["page"] = page
                taskValue["pageSize"] = pageSize
            
        }
        return .requestParameters(parameters: taskValue, encoding: URLEncoding.queryString)
    }
    
    
    var sampleData: Data {
        switch self {
            case .getTopHeadlines, .getEverything:
                return Data()
            
        }
    }
    
    var headers: [String: String]? {
        var httpHeaders = [String: String]()
        return httpHeaders
    }
}
